Akuma Language Domains:
-------------------------------------------------------------------

Language domains allows you to override the language specific domains configured
in your locale language admin settings.

When enabled, the domain setting set in the db (locale module) for a particular
language can be overriden in settings.php. You just need to add the $conf
entries as follows

    $conf['akuma_domains']['example.com'] = 'en';

This is particularly useful when you have multiple different domains for one language.
For example we have 2 domains with different themes but they both must use Russian as language:

    www.example.ru -- for desktop version
    m.example.com -- for mobile

Then config entries would be like follow:

    $conf['akuma_domains']['www.example.com'] = 'ru';
    $conf['akuma_domains']['m.example.com'] = 'ru';

GETTING STARTED
-------------------------------------------------------------------
1. Add $conf['akuma_domains']['<domain>'] = '<language prefix>'; for
   each language you have enabled and configured on your site.
2. Install 'akuma_domains' in the usual way (http://drupal.org/node/895232)
3. Flush at minimum menu caches.. or just flush them all.
4. Browse to your overridden domains.

Note: Remember to do all your sites.php and web server configurations to set up
      you domains first.

LINKS
-------------------------------------------------------------------
Project page: https://www.drupal.org/project/akuma_domains
Submit bug reports, feature suggestions: https://www.drupal.org/project/issues/akuma_domains

MAINTAINERS
-------------------------------------------------------------------
Nikita Makarov - https://www.drupal.org/user/1633480